<?php get_header(); ?>
<?php //add_filter( 'shortcode_atts_gallery', 'wpse_141896_shortcode_atts_gallery' );   ?> 
<?php //echo do_shortcode('[gallery size="full"]'); ?>
<?php $postType = ''; ?>
<div class="container webmedia">
    <div class="col-md-9">
        <?php
        if (have_posts()) {
            while (have_posts()) {
                ?>
                <?php the_post(); ?>

                <div class="mediaWrapper">
                    <?php
                    if (!has_shortcode($post->post_content, 'gallery') && !get_post_meta(get_the_ID(), 'YouTubeId', true)) {
                        //Regular Post
                        ?>
                        <h2><?php the_title(); ?></h2>
                        <?php
                        the_content();
                    } elseif (get_post_meta(get_the_ID(), 'YouTubeId', true)) {
                        $postType = 'video';
                        $youTubeId = get_post_meta(get_the_ID(), 'YouTubeId', true);
                        ?>
                        <a href="<?php echo "https://www.youtube.com/watch?v=$youTubeId" ?>">
                            <h2><?php the_title(); ?></h2>
                        </a>
                        <iframe id='ytplayer' frameborder="0" src='http://www.youtube.com/embed/<?php echo $youTubeId; ?>?origin=http://jmejpn.com' allowfullscreen></iframe>
                        <p><?php the_content() ?></p>
                        <?php
                    } else if (has_shortcode($post->post_content, 'gallery')) {
                        $postType = 'gallery';
                        ?>
                        <h2><?php the_title(); ?></h2>
                        <?php
                        $gallery = get_post_gallery($post->ID, false);
                        $ids = explode(",", $gallery['ids']);
                        $image = array();
                        foreach ($ids as $id) {
                            $images[] = wp_get_attachment_image_src($id, 'large');
                        }
                        //var_dump($images); exit();
                        foreach ($images as $image) {
                            echo "<div class='col-md-4 col-xs-12 gallery-prev'><a href='$image[0]' rel='lightbox[gallery]'><img src='$image[0]' class=''></a></div>";
                        }
                        //the_content();
                    } else if (is_single()) {
                        ?>
                        <?php $postType = 'post'; ?>
                        <h2><?php the_title(); ?></h2>
                        <?php the_content() ?>
                        <p><?php the_author_meta('description'); ?></p>
                        <?php
                    } else {
                        ?>
                        <h2><?php the_title(); ?></h2>
                        <?php the_content() ?>


                        <?php
                    }
                    ?>
                </div>
                
                    <?php comments_template(); ?>
                
                <?php
            }
        } else {
            ?>
            <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
        <?php } ?>

    </div>

    <?php $posts = get_posts(array('numberposts' => -1)); ?>
    <?php if (!empty($posts) && $postType !== '') { ?>
        <div class="col-md-3 post-list">
            <h3><?php echo get_sidebar_title($postType); ?></h3>
            <ul>
                <?php
                foreach ($posts as $post) {
                    setup_postdata($post);
                    if (get_post_meta(get_the_ID(), 'YouTubeId', true) && $postType === 'video') {
                        ?>
                        <li>
                            <a href='<?php echo get_permalink(get_the_ID()); ?>'><?php the_title(); ?></a>
                        </li>
                        <?php
                    } else if (get_post_gallery() && $postType === 'gallery') {
                        ?>
                        <li>
                            <a href='<?php echo get_permalink(get_the_ID()); ?>'><?php the_title(); ?></a>
                        </li>
                        <?php
                    } else if (!get_post_meta(get_the_ID(), 'YouTubeId', true) && !get_post_gallery() && $postType === '') {
                        ?>      
                        <li>
                            <a href='<?php echo get_permalink(get_the_ID()); ?>'><?php the_title(); ?></a>
                        </li>   
                        <?php
                    }
                }
                ?>
            </ul>

        </div>



        <?php
    }
    ?>
</div>


<?php get_footer(); ?>


<?php

function get_blog_post_type() {
    //TODO: Create function to return type of post
}

function get_sidebar_title($postType) {
    switch ($postType) {
        case 'video':
            return 'Latest Videos';
            break;
        case 'gallery':
            return 'Latest Galleries';
            break;
        case 'post':
            return 'Latest Posts';
        default:
            return '';
            break;
    }
}
?> 