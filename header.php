<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title><?php wp_title('', TRUE, 'RIGHT'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Le styles -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/css/bootstrap.min.css" type="text/css" media="all" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/css/bootstrap-theme.min.css" type="text/css" media="all" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="all" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>
<body>
    <nav class="navbar navbar-default <?php
    if (is_front_page()) {
        echo 'homenav';
    }
    ?>" role="navigation">
        <div class="container-fluid ">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand nopadding hidden-xs" href="/"><img class="" src="<?php bloginfo('template_url') ?>/logo.png"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="page_item"><a href='/'>Home</a></li>
                    <?php wp_list_pages(array('title_li' => '', 'exclude' => 4)); ?>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <?php if (is_front_page()): ?>
        <div id='front-page-banner' class="hidden-sm hidden-xs col-md-12 nopadding">

            <div class="latest-video">

                <?php $latestPost = get_latest_content(); ?>
                <?php //echo $latestPost['imageLink']; ?>
                <a href="<?php echo $latestPost['postLink']; ?>">
                    <img src='<?php echo $latestPost['imageLink'] ?>' class='col-md-12 nopadding'/>
                </a>

                <div class="videoTitle">
                    <a href="<?php echo $latestPost['postLink']; ?>">
                        <h4><?php echo $latestPost['title'] ?></h4>
                    </a>
                    <p><?php echo $latestPost['description'] ?></p>
                </div>
            </div>
        </div>
    <?php endif ?>



    <?php

    function get_latest_content() {
        $postInfo = array();
        if (have_posts()) {
            the_post();
            $postInfo['title'] = get_the_title(false);
            $excerpt = get_the_excerpt();
            $postInfo['description'] = string_limit_words($excerpt, 25);
            $postInfo['postLink'] = get_permalink(get_the_ID());
            if (get_post_meta(get_the_ID(), 'YouTubeId', true)) {
                $youTubeId = get_post_meta(get_the_ID(), 'YouTubeId', true);
                $postInfo['imageLink'] = "https://i.ytimg.com/vi/$youTubeId/mqdefault.jpg";
            } else if (!get_post_gallery() && !get_post_meta(get_the_ID(), 'YouTubeId', true)) {
                if (has_post_thumbnail()) {
                    $gallery = get_post_gallery($post->ID, false);
                    $ids = explode(",", $gallery['ids']);
                    $images = array();
                    foreach ($ids as $id) {
                        $images[] = wp_get_attachment_image_src($id, 'large');
                    }
                    $image = $images[0];
                    $postInfo['imageLink'] = $image[0];
                }
            } else if (get_post_gallery()) {
                $images = get_post_gallery_images($post->ID);
                $postInfo['imageLink'] = $images[0];
            }
        }
        //exit(var_dump($postInfo));
        return $postInfo;
    }

    function string_limit_words($string, $word_limit) {
        $words = explode(' ', $string, ($word_limit + 1));
        if (count($words) > $word_limit) {
            array_pop($words);
            //add a ... at last article when more than limit word count
            return implode(' ', $words) . "...";
        } else {
            //otherwise
            return implode(' ', $words);
        }
    }
    ?>