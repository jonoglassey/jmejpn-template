<?php
/*
Template Name: Post List
*/
?>
<?php $posts = get_posts(array('numberposts' => -1)); ?>
<?php get_header(); ?>
<div class="container">
<h1><?php the_title(); ?></h1>
<?php

foreach ($posts as $post) {
        setup_postdata($post);
        //the_post();
        //echo the_title;
        if (!get_post_gallery() && !get_post_meta(get_the_ID(), 'YouTubeId', true)) {
            ?>
            <div class='col-md-12 blog'> 
                <?php
                if (has_post_thumbnail()) {
                    $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                    echo "<img src='$url' class='col-md-4 col-sm-12 nopadding'/>";
                }
                ?>
                <div class='col-md-8'>
                    <h4><?php the_title(); ?></h4>
                    <?php the_excerpt(); ?>
                    <a href='<?php echo get_permalink(get_the_ID()); ?>'>View</a>
                </div>
            </div>

            <?php
            $i++;
        }
    }

?>
</div>
<?php get_footer(); ?>