<?php
/*
Template Name: Video List
*/
?>
<?php $posts = get_posts(array('numberposts' => -1)); ?>
<?php get_header(); ?>
<div class="container video-list">
<h1><?php the_title(); ?></h1>
<?php

$i = 0;
    foreach ($posts as $post) {
        setup_postdata($post);

            $youTubeId = get_post_meta(get_the_ID(), 'YouTubeId', true);
            if ($youTubeId) {
                ?>
                <div class="col-sm-3 featuredVideo">
                    <?php echo "<img src='https://i.ytimg.com/vi/$youTubeId/mqdefault.jpg' class='col-md-12 nopadding'/>"; ?>
                    <a href='<?php echo get_permalink(get_the_ID()); ?>'><h4><?php the_title(); ?></h4></a>
                </div>
                <?php
                $i++;
            }
        }

?>
</div>
<?php get_footer(); ?>
