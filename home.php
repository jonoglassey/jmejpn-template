<?php
$video_limit = 4;
$gallery_limit = 2;
$post_limit = 2;
$posts = get_posts(array('numberposts' => -1));
?>

<?php define('WP_DEBUG', true); ?>

<?php get_header(); ?>
<div class="container">
    <div class='videos-home col-md-4 col-sm-12 col-xs-12'>
        <div class="wrapper nopadding">
            <h1><a href="/videos">Latest Videos</a></h1>
            <?php
            $i = 0;
            foreach ($posts as $post) {
                setup_postdata($post);

                $youTubeId = get_post_meta(get_the_ID(), 'YouTubeId', true);
                if ($youTubeId && $i < $video_limit) {
                    ?>
                    <div class="col-sm-11 nopadding featuredVideo">
                        <a href='<?php echo get_permalink(get_the_ID()); ?>'>
                            <?php echo "<img src='https://i.ytimg.com/vi/$youTubeId/mqdefault.jpg' class='col-md-12 nopadding'/>"; ?>
                            <h4><?php the_title(); ?></h4>
                        </a>
                    </div>
                    <?php
                    $i++;
                }
            }
            ?>
        </div>
    </div>
    <div class='albums-home col-md-8 col-xs-12'>
        <div class="wrapper nopadding">
            <h1><a href="/albums">Newest Albums</a></h1>
            <?php
            $i = 0;
            foreach ($posts as $post) {
                setup_postdata($post);

                if (has_shortcode($post->post_content, 'gallery') && !get_post_meta(get_the_ID(), 'YouTubeId', true) && $i < $gallery_limit) {
                    //if (true):
                    $gallery = get_post_gallery($post->ID, false);
                    $ids = explode(",", $gallery['ids']);
                    $images = array();
                    foreach ($ids as $id) {
                        $images[] = wp_get_attachment_image_src($id, 'large');
                    }
                    $image = $images[0]
                    ?>
                    <div class='col-md-6 col-xs-12 featuredAlbum'>
                        <a href='<?php echo get_permalink(get_the_ID()); ?>'>
                            <?php echo "<img class='col-md-12 nopadding' src='$image[0]'/>"; ?>
                        </a>
                        <span class="col-md-12 col-xs-12 nopadding albumInfo">

                            <a href='<?php echo get_permalink(get_the_ID()); ?>'><h4><?php the_title(); ?></h4></a>
                            <p>                           
                                <?php
                                $excerpt = get_the_excerpt();
                                echo string_limit_words($excerpt, 15);
                                ?>
                            </p>
                        </span>

                    </div>

                    <?php
                    $i++;
                }
            }
            ?>
        </div>
    </div>

    <div class='blogs-home col-md-8 col-xs-12 '>
        <div class="wrapper nopadding">
            <h1><a href="/posts">Latest Blog Posts</a></h1>
            <?php
            $i = 0;

            foreach ($posts as $post) {
                setup_postdata($post);
                //the_post();
                //echo the_title;
                if (!get_post_gallery() && !get_post_meta(get_the_ID(), 'YouTubeId', true) && $i < $post_limit) {
                    ?>
                    <div class='col-md-12 blog'> 
                        <?php
                        if (has_post_thumbnail()) {
                            $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
                            echo "<img src='$url' class='col-md-4 col-sm-12 nopadding'/>";
                        }
                        ?>
                        <div class='col-md-8 col-sm-8 blogInfo'>
                            <h4><?php the_title(); ?></h4>
                            <?php the_excerpt(); ?>
                            <a href='<?php echo get_permalink(get_the_ID()); ?>'>View</a>
                        </div>
                    </div>

                    <?php
                    $i++;
                }
            }
            ?>
        </div>
    </div>
</div>
<div>
    <div class="about nopadding col-md-12">
        <img class="col-md-4 col-xs-12" src="<?php bloginfo('template_url') ?>/logo.png">
        <div class="col-md-8">
            <h2>What is JMEJPN?</h2>
            <p>JMEJPN is a YouTube Travel Series following the adventures of Jamie Goodwin & his friends throughout his time spent in Japan.
                Every Episode Jamie introduces a new location in Japan and provides background and useful advice for people looking to travel to Japan.
            </p>
            <p>
                JMEJPN is a series that is the result of a collaboration between many people. Jamie Goodwin, Jonathan Glassey, Emily Farr, Hayato Clearwater, Scott Van der Zwet are just some of the people that help bring JMEJPN to life!
            </p>
        </div>
    </div>
</div>


<?php get_footer(); ?>


