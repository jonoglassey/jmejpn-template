<?php

function wpbootstrap_scripts_with_jquery() {
    // Register the script like this for a theme:
    wp_register_script('custom-script', get_template_directory_uri() . '/bootstrap/js/bootstrap.js', array('jquery'));
    // For either a plugin or a theme, you can then enqueue the script:
    wp_enqueue_script('custom-script');
}

///**
// * Set the size attribute to 'full' in the next gallery shortcode.
// */
//function wpse_141896_shortcode_atts_gallery($out) {
//    //remove_filter( current_filter(), __FUNCTION__ );
//    $out['size'] = 'full';
//    return $out;
//}
//add_filter( 'shortcode_atts_gallery', 'wpse_141896_shortcode_atts_gallery' );

//add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );
//add_theme_support( 'post-thumbnails' );
//
//add_filter( 'wp_title', 'baw_hack_wp_title_for_home' );
//function baw_hack_wp_title_for_home( $title )
//{
//  if( empty( $title ) && ( is_home() || is_front_page() ) ) {
//    return __( 'Home', 'theme_domain' ) . ' | ' . get_bloginfo( 'name' );
//  }
//  else {
//      return $title .  ' | ' . get_bloginfo('name');
//   }
//  
//  return $title;
//}

/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 *
 * @param string $title Default title text for current view.
 * @param string $sep Optional separator.
 * @return string The filtered title.
 */
function theme_name_wp_title($title, $sep) {
    if (is_feed()) {
        return $title;
    }

    global $page, $paged;

    // Add the blog name
    $title = $title . ' | ' . get_bloginfo('name', 'display');

    // Add the blog description for the home/front page.
    $site_description = get_bloginfo('description', 'display');

    if ($site_description && ( is_home() || is_front_page() )) {
        $title = get_bloginfo('name', 'display') . " | $site_description";
    }

    // Add a page number if necessary:
    if (( $paged >= 2 || $page >= 2 ) && !is_404()) {
        $title .= " | " . sprintf(__('Page %s', '_s'), max($paged, $page));
    }

    return $title;
}

add_filter('wp_title', 'theme_name_wp_title', 10, 2);
?>