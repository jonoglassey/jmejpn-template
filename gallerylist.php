<?php
/*
  Template Name: Gallery List
 */
?>
<?php $posts = get_posts(array('numberposts' => -1)); ?>
<?php get_header(); ?>
<div class="container">
<h1><?php the_title(); ?></h1>
<?php
$i = 0;
foreach ($posts as $post) {
    setup_postdata($post);

    if (has_shortcode($post->post_content, 'gallery') && !get_post_meta(get_the_ID(), 'YouTubeId', true)) {
        //if (true):
        $images = get_post_gallery_images($post->ID);
        ?>
        <div class='col-md-4 featuredAlbum'>
            <?php echo "<img class='col-md-12 nopadding' src='$images[0]'/>"; ?>
            <div class='col-md-12 nopadding albumInfo'>
                <a href='<?php echo get_permalink(get_the_ID()); ?>'><h4><?php the_title(); ?></h4></a>
                <p><?php trim(the_excerpt()); ?></p>
            </div>
        </div>

        <?php
        $i++;
    }
}
?>
</div>

<?php get_footer(); ?>